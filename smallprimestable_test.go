// Tests for smallprimestable.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// hasDivisorInTable returns true iff there exists a number in the smallPrimesTable dividing n. Assumes that n is a non-negative integer.
func hasDivisorInTable(n int64) bool {
	for _, p := range smallPrimesTable {
		if p > n {
			return false
		} else if n%p == 0 {
			return true
		}
	}
	return false
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestSmallPrimes(t *testing.T) {
	assert := assert.New(t)
	// Check that the numbers in the table are > 1 and strictly increasing
	for i, n := range smallPrimesTable {
		assert.True(n > 1)
		if i > 0 {
			assert.True(n > smallPrimesTable[i-1])
		}
	}
	// Check that each of the numbers in the table is a prime
	m := big.NewInt(0)
	for _, n := range smallPrimesTable {
		m.SetInt64(n)
		// Note primality testing is probabilistic, so you *might* get a false
		// warning in the following assertion
		assert.True(m.SetInt64(n).ProbablyPrime(100))
	}
	// Check that maxSmallCachedPrime really is the largest value in the table
	assert.Equal(int64(maxSmallCachedPrime), smallPrimesTable[len(smallPrimesTable)-1])
	// Now we check that there aren't any gaps in the table
	for i := int64(2); i < maxSmallCachedPrime; i++ {
		assert.True(hasDivisorInTable(i))
	}
	// Test isInSmallPrimesTable
	count := 0
	idx := 0
	for i := int64(2); i <= maxSmallCachedPrime; i++ {
		if isInSmallPrimesTable(i) {
			assert.Equal(smallPrimesTable[idx], i)
			count++
			idx++
		}
	}
	assert.Equal(count, len(smallPrimesTable))
	// Test nextPrimeFromSmallPrimesTable
	idx = 0
	p := smallPrimesTable[0]
	for i := int64(0); i < maxSmallCachedPrime; i++ {
		if i == p {
			idx++
			p = smallPrimesTable[idx]
		}
		if n, err := nextPrimeFromSmallPrimesTable(i); assert.NoError(err) {
			assert.Equal(n, p)
		}
		if n, err := nextPrimeFromSmallPrimesTable(-i); assert.NoError(err) {
			assert.Equal(n, p)
		}
	}
	_, err := nextPrimeFromSmallPrimesTable(maxSmallCachedPrime)
	assert.Error(err)
	_, err = nextPrimeFromSmallPrimesTable(maxSmallCachedPrime + 10)
	assert.Error(err)
	_, err = nextPrimeFromSmallPrimesTable(-maxSmallCachedPrime)
	assert.Error(err)
	_, err = nextPrimeFromSmallPrimesTable(-maxSmallCachedPrime - 10)
	assert.Error(err)
}
