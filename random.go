// Random provides functions to generate random integers

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcasmath/integer/internal/bigintfactory"
	"crypto/rand"
	"math/big"
	"time"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// randBigInt returns a uniformly-distributed random *big.Int in the range [0, max]. The initial call may block until there is enough entropy in the system; subsequent calls will not block. It panics if max is not non-negative. Note that, unlike crypto/rand.Int, the value returned lies in the closed interval [0,max] not the open interval [0,max).
func randBigInt(max *big.Int) *big.Int {
	m := bigintfactory.New().SetInt64(1)
	m.Add(max, m)
	// read from cryto/rand until success
	for {
		if n, err := rand.Int(rand.Reader, m); err == nil {
			bigintfactory.Reuse(m)
			return n
		}
		<-time.After(500 * time.Millisecond)
	}
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Random returns a random integer in the range [0,max]. It will panic if max is negative. This wraps crypto/rand.
func Random(max *Element) *Element {
	return FromBigInt(randBigInt(max.BigInt()))
}
