// Bigintfactory maintains a factory of *big.Ints.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package bigintfactory

import (
	"math/big"
	"sync"
)

// pool is a pool of *big.Ints.
var pool = &sync.Pool{
	New: func() interface{} {
		return &big.Int{}
	},
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new *big.Int.
func New() *big.Int {
	return pool.Get().(*big.Int)
}

// Reuse makes the *big.Int n available for reuse. Warning: Do NOT use n after making it available for reuse.
func Reuse(n *big.Int) {
	if n != nil {
		pool.Put(n)
	}
}
