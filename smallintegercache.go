// Smallintegercache maintains a cache of small-valued integer elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

// The minimum and maximum integer values stored in the small-valued integers cache.
const (
	smallIntegerCacheMin = -1 << 10
	smallIntegerCacheMax = 1 << 10
)

// smallIntegerCache is a cache of Element objects associated with the small integers. To access the cached value of k, do:
//
//	var k *Element
//	if k >= smallIntegerCacheMin && k <= smallIntegerCacheMax {
//		val = &smallIntegerCache[int(k)-smallIntegerCacheMin]
//	}
var smallIntegerCache []Element

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// initSmallIntegerCache initialises the cache of small-valued Elements. This is called from the package's init function, and should NOT be called directly.
func initSmallIntegerCache() {
	smallIntegerCache = make([]Element, smallIntegerCacheMax-smallIntegerCacheMin+1)
	for i := smallIntegerCacheMin; i <= smallIntegerCacheMax; i++ {
		smallIntegerCache[i-smallIntegerCacheMin].k = int64(i)
	}
}
