// Errors defines common errors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

// objectError implements an error.
type objectError int

// Common errors.
const (
	ErrArgNotAnIntegerGreaterThan1 = objectError(iota)
	ErrArgsNotCoprime
	ErrArg1NotAnInteger
	ErrArg2NotAnInteger
	ErrArgNotAnInteger
	ErrArgOutOfRange
	ErrDivisionByZero
	ErrDecodingIntoNilObject
	ErrDecodingIntoExistingObject
	ErrEmptySlice
	ErrIllegalNegativeArg
	ErrIteratorIsClosed
	ErrIllegalNegativeExponent
	ErrIllegalZeroArg
	ErrOutOfRange
	ErrSliceNotCoprime
	ErrSliceLengthNotEqual
	ErrSliceNotPositive
	ErrStringNotAnInteger
)

/////////////////////////////////////////////////////////////////////////
// objectError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e objectError) Error() string {
	switch e {
	case ErrArgNotAnIntegerGreaterThan1:
		return "argument is not an integer >= 2"
	case ErrArgsNotCoprime:
		return "the arguments must be coprime"
	case ErrArg1NotAnInteger:
		return "argument 1 is not an integer"
	case ErrArg2NotAnInteger:
		return "argument 2 is not an integer"
	case ErrArgNotAnInteger:
		return "argument is not an integer"
	case ErrArgOutOfRange:
		return "argument out of range"
	case ErrDivisionByZero:
		return "division by zero"
	case ErrDecodingIntoNilObject:
		return "decoding into nil object"
	case ErrDecodingIntoExistingObject:
		return "decoding into existing object"
	case ErrEmptySlice:
		return "illegal empty slice"
	case ErrIllegalNegativeArg:
		return "argument must be non-negative"
	case ErrIteratorIsClosed:
		return "iterator is closed"
	case ErrIllegalNegativeExponent:
		return "exponent must be non-negative"
	case ErrIllegalZeroArg:
		return "argument must be non-zero"
	case ErrOutOfRange:
		return "result of calculation out of range"
	case ErrSliceNotCoprime:
		return "the elements of the slice must be coprime"
	case ErrSliceLengthNotEqual:
		return "the slices must be of the same length"
	case ErrSliceNotPositive:
		return "the elements of the slice must be positive"
	case ErrStringNotAnInteger:
		return "the string cannot be converted to an integer"
	default:
		return "unknown error"
	}
}

// Is return true iff target is equal to e.
func (e objectError) Is(target error) bool {
	ee, ok := target.(objectError)
	return ok && ee == e
}
