// Tests for element.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestZero_Element performs basic tests on Zero
func TestZero_Element(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// Zero should be zero
	if ok, err := zZ.AreEqual(Zero(), Zero()); assert.NoError(err) {
		assert.True(ok)
	}
	assert.True(Zero().IsEqualTo(Zero()))
	assert.True(Zero().IsEqualTo(Zero()))
	assert.True(Zero().IsEqualToInt64(0))
	// And IsZero() should be true
	if ok, err := zZ.IsZero(Zero()); assert.NoError(err) {
		assert.True(ok)
	}
	assert.True(Zero().IsZero())
	// The string representation should be "0"
	assert.Equal(Zero().String(), "0")
	// A nil variable should equate to zero
	var n *Element
	assert.True(n.IsZero())
	assert.True(n.IsEqualTo(Zero()))
	assert.True(Zero().IsEqualTo(n))
	assert.Equal(Cmp(n, Zero()), 0)
	assert.Equal(Cmp(Zero(), n), 0)
	assert.Equal(n.String(), "0")
	// 0 + 0 = 0
	assert.True(Add(Zero(), Zero()).IsZero())
	// 0 * 0 = 0
	assert.True(Multiply(Zero(), Zero()).IsZero())
	// 0 * 1 = 0
	assert.True(Multiply(Zero(), One()).IsZero())
	// 1 * 0 = 0
	assert.True(Multiply(One(), Zero()).IsZero())
}

// TestOne_Element performs basic tests on One
func TestOne_Element(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// One should be one
	if ok, err := zZ.AreEqual(One(), One()); assert.NoError(err) {
		assert.True(ok)
	}
	assert.True(One().IsEqualTo(One()))
	assert.True(One().IsEqualTo(One()))
	assert.True(One().IsEqualToInt64(1))
	// And IsOne() should be true
	if ok, err := zZ.IsOne(One()); assert.NoError(err) {
		assert.True(ok)
	}
	assert.True(One().IsOne())
	// The string representation should be "1"
	assert.Equal(One().String(), "1")
	// Zero should not be equal to one
	if ok, err := zZ.AreEqual(Zero(), One()); assert.NoError(err) {
		assert.False(ok)
	}
	assert.False(Zero().IsEqualTo(One()))
	assert.False(Zero().IsOne())
	assert.False(One().IsZero())
	// 1 + 0 = 1
	assert.True(Add(One(), Zero()).IsOne())
	// 0 + 1 = 1
	assert.True(Add(Zero(), One()).IsOne())
	// 1 * 1 = 1
	assert.True(Multiply(One(), One()).IsOne())
}

// TestFromBigIntAndReuse tests converting from a *big.Int
func TestFromBigIntAndReuse(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// 0 as a big.Int
	example0 := fromBigIntAndReuse(big.NewInt(0))
	if ok, err := zZ.AreEqual(example0, Zero()); assert.NoError(err) {
		assert.True(ok)
	}
	// 1 as a big.Int
	example1 := fromBigIntAndReuse(big.NewInt(1))
	if ok, err := zZ.AreEqual(example1, One()); assert.NoError(err) {
		assert.True(ok)
	}
	// 2^100 as a big.Int
	temp := fromBigIntAndReuse(big.NewInt(1125899906842624))
	if example2, err := zZ.Multiply(temp, temp); assert.NoError(err) {
		assert.Equal(example2.String(), "1267650600228229401496703205376")
	}
}

// TestSetBigInt tests converting to a *big.Int
func TestSetBigInt(t *testing.T) {
	assert := assert.New(t)
	// 0 to a big.Int
	n := &big.Int{}
	assert.Equal(SetBigInt(n, Zero()).Sign(), 0)
	// 1 to a big.Int
	assert.Equal(SetBigInt(n, One()).Cmp(One().BigInt()), 0)
	// 2^100 to a big.Int
	if pow2, err := FromInt(2).PowerInt64(100); assert.NoError(err) {
		assert.Equal(SetBigInt(n, pow2).String(), "1267650600228229401496703205376")
	}
}

// TestFrom tests for FromInt, FromInt32, FromInt64, and FromUInt64.
func TestFrom(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// zero
	if ok, err := zZ.AreEqual(Zero(), FromInt(0)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(Zero(), FromInt32(0)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(Zero(), FromInt64(0)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(Zero(), FromUint64(0)); assert.NoError(err) {
		assert.True(ok)
	}
	// one
	if ok, err := zZ.AreEqual(One(), FromInt(1)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(One(), FromInt32(1)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(One(), FromInt64(1)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := zZ.AreEqual(One(), FromUint64(1)); assert.NoError(err) {
		assert.True(ok)
	}
	// some values
	for _, k := range []int{-1594319, -1, 4093, 94319} {
		val := FromInt(k)
		assert.True(val.IsEqualToInt64(int64(k)))
		if kk, err := val.Int64(); assert.NoError(err) {
			assert.Equal(kk, int64(k))
		}
		if ok, err := zZ.AreEqual(val, FromInt32(int32(k))); assert.NoError(err) {
			assert.True(ok)
		}
		if ok, err := zZ.AreEqual(val, FromInt64(int64(k))); assert.NoError(err) {
			assert.True(ok)
		}
		if k >= 0 {
			if ok, err := zZ.AreEqual(val, FromUint64(uint64(k))); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
}

// TestFromString tests the FromString function.
func TestFromString(t *testing.T) {
	assert := assert.New(t)
	// The strings to test and a parallel slice of values
	strs := []string{"0", "1", "-1", "-1594319", "276401"}
	vars := []int64{0, 1, -1, -1594319, 276401}
	// Test the conversion
	for i, s := range strs {
		if n, err := FromString(s); assert.NoError(err) {
			assert.True(n.IsEqualToInt64(vars[i]))
		}
	}
	// This should fail with an error
	_, err := FromString("-1234x")
	assert.Error(err)
}

// TestMultiplyThen tests the MultiplyThenAdd and MultiplyThenSubtract functions.
func TestMultiplyThen(t *testing.T) {
	assert := assert.New(t)
	// Test values in the form {a,b,c,result}, where a * b + c = result.
	tests := [][]int64{
		[]int64{0, 3, 2, 2},
		[]int64{3, 0, 2, 2},
		[]int64{3, 2, 0, 6},
		[]int64{1, 2, 4, 6},
		[]int64{2, 1, 4, 6},
		[]int64{3, 4, -10, 2},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt64(test[0])
		b := FromInt64(test[1])
		c := FromInt64(test[2])
		assert.True(MultiplyThenAdd(a, b, c).IsEqualToInt64(test[3]))
	}
	// Test values in the form {a,b,c,result}, where a * b - c = result.
	tests = [][]int64{
		[]int64{0, 3, 2, -2},
		[]int64{3, 0, 2, -2},
		[]int64{3, 2, 0, 6},
		[]int64{1, 2, 4, -2},
		[]int64{2, 1, 4, -2},
		[]int64{3, 4, -10, 22},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt64(test[0])
		b := FromInt64(test[1])
		c := FromInt64(test[2])
		res := MultiplyThenSubtract(a, b, c)
		assert.True(res.IsEqualToInt64(test[3]), "%d*%d - %d != %s", test[0], test[1], test[2], res.String())
	}
}

// TestMultiplyMultiplyThen tests the MultiplyMultiplyThenAdd and MultiplyMultiplyThenSubtract functions.
func TestMultiplyMultiplyThen(t *testing.T) {
	assert := assert.New(t)
	// Test values in the form {a,b,c,d,result}, where a * b + c * d = result.
	tests := [][]int64{
		[]int64{0, 3, 2, 4, 8},
		[]int64{2, 4, 0, 3, 8},
		[]int64{2, 0, 0, 3, 0},
		[]int64{1, 2, 4, 6, 26},
		[]int64{2, 3, -1, 2, 4},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt64(test[0])
		b := FromInt64(test[1])
		c := FromInt64(test[2])
		d := FromInt64(test[3])
		assert.True(MultiplyMultiplyThenAdd(a, b, c, d).IsEqualToInt64(test[4]))
	}
	// Test values in the form {a,b,c,d,result}, where a * b - c * d = result.
	tests = [][]int64{
		[]int64{0, 3, 2, 4, -8},
		[]int64{2, 4, 0, 3, 8},
		[]int64{2, 0, 0, 3, 0},
		[]int64{1, 2, 4, 6, -22},
		[]int64{2, 3, -1, 2, 8},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt64(test[0])
		b := FromInt64(test[1])
		c := FromInt64(test[2])
		d := FromInt64(test[3])
		assert.True(MultiplyMultiplyThenSubtract(a, b, c, d).IsEqualToInt64(test[4]))
	}
}

// TestQuotient tests the Quotient and QuotientAndRemainder functions.
func TestQuotient(t *testing.T) {
	assert := assert.New(t)
	// Test values in the form {a,b,q,r} where a = q * b + r.
	tests := [][]int64{
		[]int64{3, 1, 3, 0},
		[]int64{0, 1, 0, 0},
		[]int64{0, 4, 0, 0},
		[]int64{3, 2, 1, 1},
		[]int64{-3, 2, -2, 1},
		[]int64{-11, -3, 3, -2},
	}
	// Run the tests
	for _, test := range tests {
		x := FromInt64(test[0])
		y := FromInt64(test[1])
		if quo, err := Quotient(x, y); assert.NoError(err) {
			assert.True(quo.IsEqualToInt64(test[2]), "%d div %d != %s", test[0], test[1], quo.String())
		}
		if quo, rem, err := QuotientAndRemainder(x, y); assert.NoError(err) {
			assert.True(quo.IsEqualToInt64(test[2]), "%d div %d != %s", test[0], test[1], quo.String())
			assert.True(rem.IsEqualToInt64(test[3]), "%d mod %d != %s", test[0], test[1], rem.String())
		}
	}
	// Division by 0 should generate an error
	_, err := Quotient(One(), Zero())
	assert.Error(err)
	_, _, err = QuotientAndRemainder(One(), Zero())
	assert.Error(err)
}

// TestSign tests the Sign method.
func TestSign(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(Zero().Sign(), 0)
	assert.Equal(One().Sign(), 1)
	assert.Equal(FromInt(7).Sign(), 1)
	assert.Equal(FromInt(-1).Sign(), -1)
	assert.Equal(FromInt(-7).Sign(), -1)
}

// TestAbs tests the Abs method.
func TestAbs(t *testing.T) {
	assert := assert.New(t)
	negative1 := One().Negate()
	assert.True(Zero().Abs().IsZero())
	assert.True(negative1.Abs().IsOne())
	assert.True(One().Abs().IsOne())
	assert.True(FromInt(7).Abs().IsEqualToInt64(7))
	assert.True(FromInt(-7).Abs().IsEqualToInt64(7))
	assert.False(FromInt(9).Abs().IsEqualToInt64(33))
	assert.False(FromInt(-9).Abs().IsEqualToInt64(33))
}

// TestScalarMultiplyByInteger tests the ScalarMultiplyByInteger, ScalarMultiplyByInt64, and ScalarMultiplyByUint64 methods.
func TestScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	// Test values in the form {n,c,result} where c * n = result.
	tests := [][]int{
		{0, 5, 0},
		{2, 0, 0},
		{3, 1, 3},
		{1, 3, 3},
		{5, 2, 10},
		{2, 5, 10},
	}
	// Run the tests
	for _, test := range tests {
		for i := -1; i <= 1; i += 2 {
			n := FromInt(test[0] * i)
			for j := -1; j <= 1; j += 2 {
				c := test[1] * j
				result := int64(test[2] * i * j)
				assert.True(n.ScalarMultiplyByInteger(FromInt(c)).IsEqualToInt64(result))
				assert.True(n.ScalarMultiplyByInt64(int64(c)).IsEqualToInt64(result))
				if c >= 0 {
					assert.True(n.ScalarMultiplyByUint64(uint64(c)).IsEqualToInt64(result))
				}
			}
		}
	}
}

// TestToInt tests the Int64, Uint64, and BigInt methods.
func TestToInt(t *testing.T) {
	assert := assert.New(t)
	// Test Int64
	if n, err := Zero().Int64(); assert.NoError(err) {
		assert.Equal(n, int64(0))
	}
	if n, err := One().Int64(); assert.NoError(err) {
		assert.Equal(n, int64(1))
	}
	if n, err := FromInt(1234).Int64(); assert.NoError(err) {
		assert.Equal(n, int64(1234))
	}
	if n, err := FromInt(-1234).Int64(); assert.NoError(err) {
		assert.Equal(n, int64(-1234))
	}
	// A large value should return an error
	if x, err := FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = x.Int64()
		assert.Error(err)
	}
	// Test Uint64
	if n, err := Zero().Uint64(); assert.NoError(err) {
		assert.Equal(n, uint64(0))
	}
	if n, err := One().Uint64(); assert.NoError(err) {
		assert.Equal(n, uint64(1))
	}
	if n, err := FromInt(1234).Uint64(); assert.NoError(err) {
		assert.Equal(n, uint64(1234))
	}
	// A negative value should return an error
	_, err := FromInt(-7).Uint64()
	assert.Error(err)
	// A large value should return an error
	if x, err := FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = x.Uint64()
		assert.Error(err)
	}
	// Test BigInt
	y := Zero().BigInt()
	assert.Equal(y.Sign(), 0)
	y = One().BigInt()
	assert.Equal(y.Cmp(One().BigInt()), 0)
	if x, err := FromInt(2).PowerInt64(100); assert.NoError(err) {
		y := x.BigInt()
		assert.Equal(y.String(), "1267650600228229401496703205376")
	}
}

// TestToInteger tests the ToInteger method.
func TestToInteger(t *testing.T) {
	assert := assert.New(t)
	// n.ToInteger() should always return n.
	for i := -5; i <= 5; i++ {
		n := FromInt(200 * i)
		if m, err := n.ToInteger(); assert.NoError(err) {
			assert.Equal(m, n)
		}
	}
}

// TestHash tests the Hash method.
func TestHash(t *testing.T) {
	assert := assert.New(t)
	// Equal values should have equal hashes
	for i := -5; i <= 5; i++ {
		n := FromInt(200 * i)
		m := FromInt(200 * i)
		assert.Equal(n.Hash(), m.Hash())
	}
	// Hash of zero should equal hash of nil
	var n *Element
	assert.Equal(n.Hash(), Zero().Hash())
}

// TestIsEvenOdd tests IsEven and IsOdd.
func TestIsEvenOdd(t *testing.T) {
	assert := assert.New(t)
	for i := -100; i <= 100; i++ {
		k := FromInt(i)
		if k.IsEven() {
			assert.True((i % 2) == 0)
		}
		if k.IsOdd() {
			assert.False((i % 2) == 0)
		}
	}
}

// TestIsDivisibleBy tests IsDivisibleBy
func TestIsDivisibleBy(t *testing.T) {
	assert := assert.New(t)
	tests := [][]int{
		{12, 5},
		{12, 4},
		{12, 3},
		{12, 2},
		{12, 1},
		{0, 1},
	}
	// Run the tests
	for _, test := range tests {
		for i := -1; i <= 1; i += 2 {
			a := i * test[0]
			aa := FromInt(a)
			for j := -1; j <= 1; j += 2 {
				b := j * test[1]
				if result, err := aa.IsDivisibleBy(FromInt(b)); assert.NoError(err) {
					assert.Equal(result, a%b == 0)
				}
			}
		}
	}
	// Division by 0 should return an error
	_, err := FromInt(12).IsDivisibleBy(Zero())
	assert.Error(err)
	_, err = Zero().IsDivisibleBy(Zero())
	assert.Error(err)
}

// TestMod tests Mod
func TestMod(t *testing.T) {
	assert := assert.New(t)
	// The tests are of the form {m,n,nmod} where m is the modulus, n the
	// integer to test, and nmod = n mod m.
	tests := [][]int{
		{1, 7, 0},
		{1, 0, 0},
		{1, -7, 0},
		{2, 7, 1},
		{2, 0, 0},
		{2, -7, 1},
		{3, 7, 1},
		{3, 8, 2},
		{3, 9, 0},
		{3, 0, 0},
		{3, -7, 2},
		{3, -8, 1},
		{3, -9, 0},
		{-1, 7, 0},
		{-1, 0, 0},
		{-1, -7, 0},
		{-2, 7, -1},
		{-2, 0, 0},
		{-2, -7, -1},
		{-3, 7, -2},
		{-3, 8, -1},
		{-3, 9, 0},
		{-3, 0, 0},
		{-3, -7, -1},
		{-3, -8, -2},
		{-3, -9, 0},
	}
	// Run the tests
	for _, test := range tests {
		if nmod, err := FromInt(test[1]).Mod(FromInt(test[0])); assert.NoError(err) {
			assert.True(nmod.IsEqualToInt64(int64(test[2])))
		}
	}
	// Asking for n mod 0 should error
	_, err := One().Mod(Zero())
	assert.Error(err)
}

// TestInverseMod tests InverseMod
func TestInverseMod(t *testing.T) {
	assert := assert.New(t)
	// The tests are of the form {m,n,ninv}, where m is the modulus, n the
	// integer to test, and n * ninv = 1 mod m.
	tests := [][]int{
		{3, 8, 2},
		{4, 15, 3},
		{5, -3, 3},
		{6, 11, 5},
		{7, 51, 4},
		{8, 87, 7},
		{9, -16, 5},
		{-3, 8, -1},
		{-4, 15, -1},
		{-5, -3, -2},
		{-6, 11, -1},
		{-7, 51, -3},
		{-8, 87, -1},
		{-9, -16, -4},
	}
	// Run the tests
	for _, test := range tests {
		if ninv, err := FromInt(test[1]).InverseMod(FromInt(test[0])); assert.NoError(err, "[%d mod %d]", test[1], test[0]) {
			assert.True(ninv.IsEqualToInt64(int64(test[2])), "%d^-1 mod %d != %s", test[1], test[0], ninv.String())
		}
	}
	// Asking for n and m non-coprime should error
	_, err := Zero().InverseMod(FromInt(3))
	assert.Error(err)
	_, err = Zero().InverseMod(Zero())
	assert.Error(err)
	_, err = FromInt(5).InverseMod(Zero())
	assert.Error(err)
	_, err = One().InverseMod(One())
	assert.Error(err)
	_, err = FromInt(3).InverseMod(One())
	assert.Error(err)
	_, err = FromInt(7).InverseMod(FromInt(14))
	assert.Error(err)
}
