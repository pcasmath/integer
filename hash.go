// Hash contains hashing functions for integers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcasmath/integer/internal/bigintfactory"
	"bitbucket.org/pcasmath/object"
	"math"
	"math/big"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// hashInt32 returns the hash of the given integer.
func hashInt32(c int32) uint32 {
	return object.CombineHash(uint32(c), 0)
}

// hashInt64 returns the hash of the given integer.
func hashInt64(c int64) uint32 {
	return object.CombineHash(uint32(c), uint32(c>>32))
}

// hashInt returns the hash of the given integer.
func hashInt(c int) uint32 {
	return hashInt64(int64(c))
}

// hashUint64 returns the hash of the given unsigned integer.
func hashUint64(c uint64) uint32 {
	return object.CombineHash(uint32(c), uint32(c>>32))
}

// hashBigInt returns the hash of the given big integer.
func hashBigInt(n *big.Int) uint32 {
	x := bigintfactory.New().SetInt64(math.MaxInt64)
	y := bigintfactory.New().Mod(n, x)
	bigintfactory.Reuse(x)
	k := y.Int64()
	bigintfactory.Reuse(y)
	return hashInt64(k)
}
