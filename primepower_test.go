// Tests for primepower.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestIsPrimePowerInt64 tests IsPrimePowerInt64.
func TestIsPrimePowerInt64(t *testing.T) {
	assert := assert.New(t)
	// We collect some statistics about prime powers
	count := make([]int, 64)
	for n := int64(2); n <= 20000; n++ {
		if ok, _, k, err := IsPrimePowerInt64(n); assert.NoError(err) {
			if ok {
				count[k]++
			} else {
				count[0]++
			}
		}
	}
	// Compare these results against the pre-computed correct answers
	expectedCount := [...]int{
		17671, 2262, 34, 9, 5, 4, 3, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	}
	for i, num := range expectedCount {
		assert.Equal(num, count[i])
	}
	// An argument < 2 should generate an error
	_, _, _, err := IsPrimePowerInt64(1)
	assert.Error(err)
	_, _, _, err = IsPrimePowerInt64(0)
	assert.Error(err)
	_, _, _, err = IsPrimePowerInt64(-1)
	assert.Error(err)
}
