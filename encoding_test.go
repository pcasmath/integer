// Tests of integer gob encoding/decoding.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bytes"
	"encoding/gob"
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobEncode tests gob encoding/decoding.
func TestGobEncode(t *testing.T) {
	assert := assert.New(t)
	// Work through some integers
	for k := -14; k <= 14; k++ {
		val := FromInt(k)
		// Create the encoder and decoder
		var b bytes.Buffer
		enc := gob.NewEncoder(&b)
		dec := gob.NewDecoder(&b)
		// Encode the integer
		if assert.NoError(enc.Encode(val)) {
			// Decode the integer
			if decval, err := GobDecode(dec); assert.NoError(err) {
				assert.True(val.IsEqualTo(decval))
			}
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var n *Element
	if b, err := n.GobEncode(); assert.NoError(err) {
		// Decoding should give zero
		m := &Element{}
		if assert.NoError(m.GobDecode(b)) {
			assert.True(m.IsZero())
		}
	}
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	assert := assert.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode the an integer
	if assert.NoError(enc.Encode(One())) {
		// Try to decode directly into a nil object -- this should error
		var m *Element
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	assert := assert.New(t)
	// Create the byte slice: the first byte is the version number, which we
	// set to "future" version.
	b := []byte{encodingVersion + 1}
	// Try to decode as a *Element -- this should error
	m := &Element{}
	assert.Error(m.GobDecode(b))
}

// TestGobDecodeExisting tests gob decoding into an existing integer.
func TestGobDecodeExisting(t *testing.T) {
	assert := assert.New(t)
	// Create two integers
	n := FromInt(12)
	m := FromInt(7)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode the integer
	if assert.NoError(enc.Encode(n)) {
		// Try to decode directly into m -- this should error
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	assert := assert.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
	// Try to decode as a []*Element -- this should error
	_, err = GobDecodeSlice(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
}

// TestGobSliceEncoding tests gob encoding/decoding of slices.
func TestGobSliceEncoding(t *testing.T) {
	assert := assert.New(t)
	// Create the slice of integers
	T := []int{-5, 2, 0, 0, 0, 0, 0, 3, 0, 0, 1}
	S := make([]*Element, 0, len(T))
	for _, t := range T {
		S = append(S, FromInt(t))
	}
	// Create the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	// Encode the slice of integers
	if assert.NoError(enc.Encode(S)) {
		// Decode the slice of integers
		if SS, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Len(SS, len(S)) {
			// Check for equality
			for i, s := range S {
				assert.True(s.IsEqualTo(SS[i]))
			}
		}
	}
}
