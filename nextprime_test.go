// Tests for nextprimes.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

// TestNextPrime tests NthPrimeInt64, NextPrimeInt64, and NextPrime.
func TestNextPrime(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// Check a range of primes small primes
	for N := int64(2000); N <= 3000; N++ {
		q, err := NthPrimeInt64(N - 1)
		require.NoError(err)
		p, err := NthPrimeInt64(N)
		require.NoError(err)
		for i := int64(q); i <= p-1; i++ {
			if m, err := NextPrimeInt64(i); assert.NoError(err) {
				assert.Equal(m, p)
			}
			m := NextPrime(FromInt64(i))
			assert.True(m.IsEqualToInt64(p))
		}
	}
	// Check the ends
	maxPrime := int64(9223372036854775783)
	if result, err := NextPrimeInt64(maxPrime - 1); assert.NoError(err) {
		assert.Equal(result, maxPrime)
	}
	_, err := NextPrimeInt64(maxPrime)
	assert.Error(err)
	_, err = NextPrimeInt64(maxPrime + 1)
	assert.Error(err)
	if result, err := NextPrimeInt64(-maxPrime + 1); assert.NoError(err) {
		assert.Equal(result, maxPrime)
	}
	_, err = NextPrimeInt64(-maxPrime)
	assert.Error(err)
	_, err = NextPrimeInt64(-maxPrime - 1)
	assert.Error(err)
	// Check the next prime after maxPrime - 1 is maxPrime.
	assert.True(NextPrime(FromInt64(maxPrime - 1)).IsEqualToInt64(maxPrime))
	// Now check the next few primes
	tests := []string{
		"9223372036854775837",
		"9223372036854775907",
		"9223372036854775931",
		"9223372036854775939",
		"9223372036854775963",
		"9223372036854776063",
		"9223372036854776077",
		"9223372036854776167",
	}
	lastPrime := FromInt64(maxPrime)
	for _, test := range tests {
		answer, err := FromString(test)
		require.NoError(err)
		assert.True(NextPrime(lastPrime).IsEqualTo(answer))
		assert.True(NextPrime(lastPrime.Increment()).IsEqualTo(answer))
		assert.True(NextPrime(answer.Decrement()).IsEqualTo(answer))
		lastPrime = answer
	}
}

// TestPrimeIteratorInt64 tests the PrimeIteratorInt64.
func TestPrimeIteratorInt64(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// Check that the iterator agrees with NextPrimeInt64
	np := NewPrimeIteratorInt64(500)
	p := int64(500)
	for i := 0; i < len(smallPrimesTable)+1000; i++ {
		var err error
		p, err = NextPrimeInt64(p)
		require.NoError(err)
		var q int64
		q, err = np.Next()
		require.NoError(err)
		assert.Equal(p, q)
	}
	np.Close()
	// Check that iteration halts at the max prime
	np = NewPrimeIteratorInt64(maxPrimeInt64 - 1)
	var err error
	p, err = np.Next()
	require.NoError(err)
	assert.Equal(p, int64(maxPrimeInt64))
	_, err = np.Next()
	assert.Error(err)
	np.Close()
	// Check that iteration doesn't get started if we begin at max prime
	np = NewPrimeIteratorInt64(maxPrimeInt64)
	_, err = np.Next()
	assert.Error(err)
	np.Close()
}
