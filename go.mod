module bitbucket.org/pcasmath/integer

go 1.16

require (
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/slice v0.0.4
	bitbucket.org/pcastools/gobutil v1.0.3
	bitbucket.org/pcastools/mathutil v1.0.4
	bitbucket.org/pcastools/slice v1.0.3
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/testify v1.7.0
)
