// Tests for chineseremainder.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestChineseRemainder(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// Some straightforward tests
	tests := [][][]string{
		{
			{"2", "3", "1"},
			{"3", "4", "5"},
			{"11"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"39739698249358", "408149442487193", "857034448238765", "205493732848557", "494731949110853"},
			{"307883327591819509938394827916127863595443616641506243157845168679493864"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"65918145520595", "421895226541463", "95331522179347", "439659938360313", "268426215612686"},
			{"80435385046025345100070882979498545536540500862588310496927073165871064"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"683554083372661", "878941804617451", "117146342260754", "743937041912773", "830500336883085"},
			{"29458572485067081526978191160647454202331538075127644809923780232877278385"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"254649352674151", "260346158598794", "112723234145151", "856487992220057", "720750091138447"},
			{"4143537547097048687906911488432543767122848483118652460492255976611443758"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"903879639793883", "849257963854957", "822478182921370", "441370534416513", "931792238483273"},
			{"175622489479172987739396582991972925290155104506026100449851715927556806599"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"198246437517395", "438450455386178", "677787479175861", "606959786003273", "498096496473377"},
			{"15955174791788603430067837588098823004406895631029133160591998278613974024"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"960275161730293", "483216280268881", "686912267913333", "51660898129747", "1083307698607643"},
			{"16250386019016746735159476256270200430223420654752435413087138841471320036"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"19383372877891", "690049014380375", "824156873011801", "136795815083591", "626427346960313"},
			{"805403016092564477443684866011354153930803215328488430100325522885926537"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"226584122026827", "258108287728561", "721133296164367", "494794167620452", "810929922173593"},
			{"14786129483499341652664367138004436608477397776517133785475377559965286291"},
		},
		{
			{"572485665637214", "719357669357162", "1042648444381759", "526956379431659", "255443230157920"},
			{"548168107423435", "949088031279421", "1007433009553901", "977739342829631", "1031270343771369"},
			{"89133830934744664714987181064173837331791088959561605656789493269678894034"},
		},
	}
	for _, test := range tests {
		residues := make([]*Element, 0, len(test[0]))
		for _, s := range test[0] {
			val, err := FromString(s)
			require.NoError(err)
			residues = append(residues, val)
		}
		moduli := make([]*Element, 0, len(test[1]))
		for _, s := range test[1] {
			val, err := FromString(s)
			require.NoError(err)
			moduli = append(moduli, val)
		}
		expected, err := FromString(test[2][0])
		require.NoError(err)
		if got, err := ChineseRemainderTheorem(residues, moduli); assert.NoError(err) {
			assert.True(expected.IsEqualTo(got))
		}
	}
	// The moduli must be positive and coprime
	tests = [][][]string{
		{
			{"2", "0", "1"},
			{"2", "3", "9"},
		},
		{
			{"2", "0", "1"},
			{"-2", "3", "5"},
		},
		{
			{"2", "0", "1"},
			{"2", "0", "9"},
		},
	}
	for _, test := range tests {
		residues := make([]*Element, 0, len(test[0]))
		for _, s := range test[0] {
			val, err := FromString(s)
			require.NoError(err)
			residues = append(residues, val)
		}
		moduli := make([]*Element, 0, len(test[1]))
		for _, s := range test[1] {
			val, err := FromString(s)
			require.NoError(err)
			moduli = append(moduli, val)
		}
		_, err := ChineseRemainderTheorem(residues, moduli)
		assert.Error(err)
	}
}

func TestPrepareChineseRemainderTheorem(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// We use the primes 2, 3, 5 and check that we recover the values 0..29
	vals := []int64{0, 6, 12, 18, 24, 10, 16, 22, 28, 4, 20, 26, 2, 8, 14, 15,
		21, 27, 3, 9, 25, 1, 7, 13, 19, 5, 11, 17, 23, 29}
	crt, err := PrepareChineseRemainderTheorem(FromInt(2), FromInt(3), FromInt(5))
	require.NoError(err)
	idx := 0
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			for k := 0; k < 5; k++ {
				if res, err := crt(FromInt(i), FromInt(j), FromInt(k)); assert.NoError(err) {
					assert.True(res.IsEqualToInt64(vals[idx]))
				}
				idx++
			}
		}
	}
}
