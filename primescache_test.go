// Tests for primes_cache.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////////////////

func TestNthPrime(t *testing.T) {
	assert := assert.New(t)
	// The test values in the form {N,p}.
	tests := [][]int64{
		{1, 2},
		{2, 3},
		{3, 5},
		{2866, 26053},
		{7903, 80749},
		{17991, 200041},
		{21380, 241883},
		{22077, 250499},
		{25990, 299909},
		{29915, 349291},
		{32613, 384113},
		{36799, 438133},
		{47564, 579673},
		{50334, 616313},
		{57831, 717113},
		{61826, 771209},
		{66772, 838421},
		{67679, 850853},
		{71082, 897251},
		{77047, 979553},
		{82212, 1051181},
		{83409, 1068149},
		{84945, 1089457},
		{40182, 482281},
		{10457, 110051},
		{40977, 492781},
		{68928, 867773},
		{85582, 1098077},
		{47210, 575087},
		{96878, 1255829},
		{28642, 333031},
		{54885, 677737},
		{28908, 336463},
		{1219, 9883},
		{69351, 873619},
		{64932, 813343},
		{45280, 549449},
		{69068, 869597},
		{94466, 1221653},
		{36277, 431479},
		{49724, 608303},
		{2169, 19087},
		{87769, 1128509},
	}
	////////////////////////////////////////////////////////////////////////////////
	// tests for NthPrime
	////////////////////////////////////////////////////////////////////////////////
	// Run the tests
	for _, test := range tests {
		if result, err := NthPrime(test[0]); assert.NoError(err) {
			assert.True(result.IsEqualToInt64(test[1]))
		}
	}
	// The maximum input value should be int64(len(nthPrimeSeed)+1)*nthPrimeBlockSize
	maxInputValue := int64(len(nthPrimeSeed)+1) * nthPrimeBlockSize
	maxOutputValue := int64(1742537)
	if result, err := NthPrime(maxInputValue); assert.NoError(err) {
		assert.True(result.IsEqualToInt64(maxOutputValue))
	}
	_, err := NthPrime(maxInputValue + 1)
	assert.Error(err)
	////////////////////////////////////////////////////////////////////////////////
	// tests for NthPrimeInt64
	////////////////////////////////////////////////////////////////////////////////
	// Run the tests
	for _, test := range tests {
		if result, err := NthPrimeInt64(test[0]); assert.NoError(err) {
			assert.Equal(result, test[1])
		}
	}
	// The maximum input value should be maxInputValue
	if result, err := NthPrimeInt64(maxInputValue); assert.NoError(err) {
		assert.Equal(result, maxOutputValue)
	}
	_, err = NthPrimeInt64(maxInputValue + 1)
	assert.Error(err)
}
